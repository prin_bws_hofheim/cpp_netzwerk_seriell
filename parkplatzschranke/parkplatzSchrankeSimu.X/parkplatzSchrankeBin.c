//#ifndef __QUASSELSTRIPPE_ASCII_C
//#define __QUASSELSTRIPPE_ASCII_C
/*
* quasselstrippe_bin.c
*
* Created: 11.09.2021
* Author : Thomas
*
* Binär Version des Protokolls
*
* 0x01 echo: bekommt string, sendet ihn 1:1 zurück. (noch nicht implementiert!)
* 0x02 add: bekommt eine Zahl (1 Byte) inc um 1, sendet ihn zurück.
* 0x03 btn: bei Betätigen des Tasters wird der letzte Wert der Zahl aus 0x02 geschickt (ohne inc)
* 0x04 led: schaltet die LED (Val '5' (0x35) ein, sonst aus)
*
* 0x01 echo: bekommt string, sendet ihn 1:1 zurück.
* PC (Auftrag): 0x01 0xnn 0x... <nn Bytes, max 31>
* uC (Antwort): 0x01 0xnn 0x... <nn Bytes, max 31>
* PC sendet "Helo": 0x01 0x04 0x48 0x65 0x6C 0x6F
* uC Antwort: 0x01 0x04 0x48 0x65 0x6C 0x6F
*
* 0x02 add: bekommt eine Zahl (1 Byte) inc um 1, sendet ihn zurück.
* PC (Auftrag): 0x02 0xnn
* uC (Antwort): 0x02 0xnn+1
* PC sendet  "41": 0x02 0x29
* uC Antwort "42": 0x01 0x2A
*
* 0x03 btn: bei Betätigen des Tasters wird der letzte Wert der Zahl aus 0x02 geschickt (ohne inc)
* uC (Meldung): 0x03 0xnn (Startwert: 0x00)
* uC hat als letzten Wert 0x29 empfangen, 0x2A gesendet
* uC sendet 0x03 0x2B
*
* 0x04 led: schaltet die LED (Val '5' (0x35) ein, sonst aus)
* PC (Auftrag): 0x04 0xnn (n = 0x35(ASCII '5') ein, sonst aus)
*
* 0x05 led: schaltet die LED ein
* PC (Auftrag): 0x05
*
* 0x06 led: schaltet die LED aus
* PC (Auftrag): 0x06
*/

#include "parkplatzSchrankeDefinesGlobalVars.h"

char reqEchoRecBin(unsigned char inByte);
char reqEchoSendBin();
void reqLedToggleBin(unsigned char recValue);
void reqLedOnBin();
void reqLedOffBin();
void reqSendLogBin();
void reqClearLogBin();

unsigned char recDataBin()
{
    unsigned char inByte = 0x00;
    char swichOn = 0x00;
    
    while(!(UCSR0A & (1<<RXC0)))
    {
        if(!(PINB & (1<<PB7)))
        {
            if(!swichOn)
            {
                if(PORTB & (1<<PB5))
                PORTB &=~(1<<PB5);
                else
                PORTB |=(1<<PB5);

                UDR0 = 0x03;
                swichOn = 1;
            }
        }
        else
            swichOn = 0;
    }
    while(!(UCSR0A & (1<<UDRE0)))
    ;
    inByte = UDR0;
    recLog[recLogCounter] = inByte;
    recLogCounter++;
    
    return inByte;
}

unsigned char initProtocollBin()
{
    unsigned char inByte = 0x00;
    unsigned char stage = 0x00;
    
    inByte = recDataBin();
    if(0x02 == inByte)
        stage ++;
    
    inByte = recDataBin();
    if(0x02 == inByte){
        stage ++;    
        return 0x01;
    }
    
    return 0x00;
}

void reqLedToggleBin(unsigned char recValue)
{
	if(0x35==recValue)
	PORTB &=~(1<<PB5);
	else
	PORTB |= (1<<PB5);
}

void reqLedOnBin()
{
	PORTB |= (1<<PB5);
}

void reqLedOffBin()
{
	PORTB &=~(1<<PB5);
}

void reqSendLogBin()
{
	for(int i = 0; i < recLogCounter; i++)
	{
		while(!(UCSR0A & (1<<UDRE0)))
		{
		}
		UDR0 = recLog[i];
	}
}

void reqClearLogBin()
{
	for(int i = 0; i < recLogCounter; i++)
	{
		recLog[i] = 0x00;
		recLogCounter = 0x00;
	}
}

void processProtocollBin()
{
	int requestStage = 0x00;
	unsigned char lenByte = 0x00;
    unsigned char commandByte = 0x00;

	while (1)
	{
        lenByte = recDataBin();        
        commandByte = recDataBin();
        
        switch(inByte)
        {
            case 0x31:
            requestStage = 0x00;
            break;

            case 0x32:
            reqLedOnBin();
            requestStage = 0x00;
            break;

            case 0x33:
            reqLedOffBin();
            requestStage = 0x00;
            break;

            case 0x34:
            reqSendLogBin();
            break;

            case 0x35:
            reqClearLogBin();
            break;

            case 0x36:
            reqClearLogBin();
            break;
            default:
            {
            }
        }
	}
}
//#endif //__QUASSELSTRIPPE_ASCII_C