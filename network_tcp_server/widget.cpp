#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    tcpServer = new QTcpServer(this);
    tcpSocket = nullptr;
    QObject::connect(tcpServer, &QTcpServer::newConnection, this, &Widget::newConnection);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::newConnection()
{
    qDebug() << "new client connected";
    tcpSocket = tcpServer->nextPendingConnection();
    QObject::connect(tcpSocket, &QTcpSocket::readyRead, this, &Widget::readData);
}


void Widget::readData()
{
    QString sendText;

    sendText = "HTTP/1.1 200 OK\n Server: homebrew/1.0 (Unix)\n Content-Length: 0\nContent-Language: de\n Connection: close\n Content-Type: text/html";

    qDebug() << "recieved: " <<  tcpSocket->readLine();
    tcpSocket->write(sendText.toLatin1());
    qDebug() << "send: " << sendText;
}

void Widget::on_btnTcpServerListen_clicked()
{
    tcpServer->listen(QHostAddress::Any, 4711);
    qDebug() << "TcpServer is listening: " << tcpServer->isListening();
}
