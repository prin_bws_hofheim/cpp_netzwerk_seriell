#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    tcpSocket = new QTcpSocket(this);
    QObject::connect(tcpSocket, &QTcpSocket::readyRead, this, & Widget::readData);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnSendText_clicked()
{
    QString sendText;

    sendText = ui->edtSendText->text();

    tcpSocket->write(sendText.toLatin1());
    qDebug() << sendText;
}

void Widget::on_btnConnectServer_clicked()
{
    tcpSocket->connectToHost("localhost", 4711);
}

void Widget::readData()
{
    QString receivedText;

    receivedText = tcpSocket->readLine();
    qDebug() << "received: " <<  receivedText;
    ui->edtReceive->appendPlainText(receivedText);
}
