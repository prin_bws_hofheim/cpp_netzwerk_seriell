#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget){
    ui->setupUi(this);

    factor = 16;
}

Widget::~Widget(){
    delete ui;
}

void Widget::on_btnOperate_clicked(){
    QString textDataIn;
    QString sendText;
    QString outString;
    QStringList listDataIn;
    unsigned int sum =0;

    textDataIn = ui->edtDataIn->toPlainText();
    textDataIn = textDataIn.replace(" ", "\n");
    listDataIn = textDataIn.split("\n");

    for(int i = 0; i < listDataIn.size(); i++){
        qDebug() << listDataIn[i];

        if(i%2)
            sum += 3 * listDataIn[i].toUInt(nullptr, factor);
        else
            sum += listDataIn[i].toUInt(nullptr, factor);
        qDebug() << sum;
    }

    sum = sum % 0x100;
    outString = formatHexString(QString::number(sum, 16));
    ui->edtCheckSum->setText(outString);
    qDebug() << sum;

    outString = formatHexString(QString::number(listDataIn.size()+1, 16));
    sendText = outString;

    for(int i = 0; i < listDataIn.size(); i++){
        sendText.append(" ");
        if(10 == factor){
            outString = formatHexString(QString::number(listDataIn[i].toInt(nullptr, factor), 16));
            sendText.append(outString);
        }
        else
            sendText.append(listDataIn[i]);
    }
    sendText.append(" ");
    outString = formatHexString(QString::number(sum, 16));
    sendText.append(outString);

    ui->edtSendText->setText(sendText);
}

QString Widget::formatHexString(QString inString) {
    QString retString;

    if(inString.size() == 0){
        retString = "0x00";
    }
    if(inString.size() == 1){
        retString = "0x0" + inString;
    }
    if(inString.size() == 2){
        retString = "0x" + inString;
    }
    return retString;
}

void Widget::on_btnHexValues_clicked()
{
    factor = 16;
}

void Widget::on_btnDecValues_clicked()
{
    factor = 10;
}
