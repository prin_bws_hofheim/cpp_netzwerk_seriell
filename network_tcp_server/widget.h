#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTcpServer>
#include <QTcpSocket>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void newConnection();
    void readData();

    void on_btnTcpServerListen_clicked();

private:
    Ui::Widget *ui;

    QTcpServer* tcpServer;
    QTcpSocket* tcpSocket;
};

#endif // WIDGET_H
